<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class requestController extends Controller
{
    public function Done(){
        return response()->json(array(
            'code'      =>  200,
            'message'   =>  'Your Request Done '
        ), 200);
    }
    public function Unouthrized(){
        return response()->json(array(
            'code'      =>  403,
            'message'   =>  'Your Request Not Allowed '
        ), 403);
    }
    public function DoneWithData($request){
        return response()->json(array(
            'code'      =>  200,
            'message'   =>  'Your Request Done',
            'data'=>$request
        ), 200);
    }
    public function ErrorResponse($code=500,$request){
        return response()->json(array(
            'code'      =>  $code,
            'message'   =>  'Error ',
            'data'=> $request
        ), $code);
    }

}
