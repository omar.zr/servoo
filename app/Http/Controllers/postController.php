<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Controllers\requestController;
use Illuminate\Http\Request;

class postController extends Controller
{
    public function __construct(requestController $handler)
    {
        $this->handler = $handler;
    }

    public function creat(Request $req)
    {
       $req->validate([
            'title'=>'string|max:250|required',
            'type'=>'required|string'
       ]);
       $req->sender_id = auth('api')->user()->id;
       $post = new Post($req->all());
       $post->save();

       return $this->handler->DoneWithData($post);

    }

    public function index()
    {
        return Post::orderBy('id', 'ASC')->get();
    }


    public function filters(Request $req)
    {


    }





}
