<?php

namespace App\Http\Controllers;
use App\User;
use App\TempPhone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\requestController;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct(requestController $handler)
{
    $this->handler = $handler;
}

 public function login(Request $request)
{
    $request->validate([
        'phone' => 'required|numeric',
        'password' => 'required'
    ]);

    if( Auth::attempt(['phone'=>$request->phone, 'password'=>$request->password]) ) {

        $user = Auth::user();
        $token = $user->createToken($user->phone.'-'.now());
        $ob = new \stdClass();
        $ob->user = $user;
        $ob->token = $token->accessToken;
        // $ob= json_encode($ob) ;
        return $this->handler->DoneWithData($ob);
    }
}
public function register(Request $req)
{
    $req->validate([
        'name' => 'required|max:55',
        'password' => 'required|min:8',
        'phone' => 'required|numeric',
    ]);
    if (TempPhone::where('phone',$req->phone)->orderBy('id', 'ASC')->get()->count()==0)
    return $this->handler->ErrorResponse(404,['message'=>'Number Not Exist']);

    if(!TempPhone::where('phone',$req->phone)->orderBy('id', 'ASC')->get()->last()->verified){
        return $this->handler->ErrorResponse(401,['message'=>'Number Not Verified']);
    }

    $user = new User();
    $user->name= $req->name;
    $user->email= $req->name."@servoo.com";
    $user->password=  Hash::make($user->password);
    $user->phone= $req->phone;
    $user->save();
    return $this->handler->DoneWithData($user);

}
public function sendCode(Request $req)
{
    $tries = TempPhone::where('device_id',$req->device_id)->orderBy('id', 'ASC')->get();
    // return $tries;
    if($tries->count()==0){
        $temp = new TempPhone();
        $temp->phone= $req->phone;
        $temp->device_id= $req->device_id;
        $temp->code= rand(1000,9999);
        #----------------Send Code Using SMS-------------
        $temp->save();
    return $this->handler->DoneWithData(['message'=>'Code Sent']);
    }elseif($tries->count()>5){
        return $this->handler->DoneWithData(['message'=>'Your Number Banned']);
    }else{
        $temp= $tries->last();
        $duration =$temp->created_at->diffInMinutes(now());
        if($duration<5){
            return $this->handler->DoneWithData(['message'=>'Please Wait At Least 5 Minutes To Request Another Code']);
        }
        $temp = new TempPhone();
        $temp->phone= $req->phone;
        $temp->device_id= $req->device_id;
        $temp->code= rand(1000,9999);
        #----------------Send Code Using SMS-------------
        $temp->save();
    return $this->handler->DoneWithData(['message'=>'Code Sent']);
    }


}
public function verifyCode(Request $req)
{
    $temp = TempPhone::where('phone',$req->phone)->orderBy('id', 'ASC')->get()->last();
    if($temp->code == $req->code){
        $temp->verified=1;
        $temp->save();
        return $this->handler->DoneWithData(['message'=>'Code Correct']);

    }
    return $this->handler->ErrorResponse(406,['message'=>'Code Wrong']);


}
public function editPersonalInfo(Request $req)
{
    return auth('api')->user();
}



}
