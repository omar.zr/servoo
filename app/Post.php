<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['title','type','from','to','weight','package_type','end_date','sender_id','receiver_id','cost'];

}
