<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempPhone extends Model
{
    protected $table = 'temp_phones';

    protected $fillable = ['code', 'phone','device_id'];
    protected $hidden= ['code'];
}
